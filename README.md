Reads the the scores of a certain number of students in  subjects

A > B denotes A has scored more than B in all subjects

A < B denotes A has scored less than B in all subjects

A # B denotes that the values are not comparable as A has scored less than B in some subjects and more than B in other


Outputs the least number of lines required in the above form to represent the state of values completely

Use OOPs for the program


Roles

1. Sumithra - Create README, read data, extract list of all possible relations

2. Akshaya - Create Object class, remove redundancies and reduce relations 
