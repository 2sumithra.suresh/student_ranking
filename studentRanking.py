import sys
from itertools import combinations as nCr

COMMENT = "#"


def load_data(filename: str) -> list[tuple[str, list[int]]]:
    data = []
    with open(filename) as f:
        for line in f:
            if line[0] != COMMENT:
                parts = line.split()
                name = parts[0]
                raw_marks = list(map(int, parts[1:]))
                data.append((name, raw_marks))
    return data


def relation(a_student: tuple[str, list[int]], b_student: tuple[str, list[int]]) -> tuple[str, str]:
    a_name, a_marks = a_student
    b_name, b_marks = b_student

    if all(a < b for (a, b) in zip(a_marks, b_marks)):
        return a_name, b_name
    elif all(a > b for (a, b) in zip(a_marks, b_marks)):
        return b_name, a_name
    else:
        return None


class Relations:
    def __init__(self, file_name: str):
        self.file_name = file_name
        self.data = load_data(file_name)
        self.relations = set()
        self.irreducible = set()
        self.only_first = set()

    def all_relations(self) -> None:
        self.relations = {relation(*pair) for pair in nCr(self.data, 2) if relation(*pair)}

    def irreducibles(self) -> None:
        self.all_relations()
        firsts = {r[0] for r in self.relations}
        seconds = {r[1] for r in self.relations}
        both = firsts & seconds
        self.irreducible = self.relations.copy()

        for f, s in self.relations:
            if any((f, b) in self.relations and (b, s) in self.relations for b in both):
                self.irreducible.discard((f, s))

    def only_firsts(self) -> set[str]:
        self.irreducibles()
        firsts = {r[0] for r in self.irreducible}
        seconds = {r[1] for r in self.irreducible}
        both = firsts & seconds
        self.only_first = firsts - both
        return self.only_first

    def combine(self, start: str) -> list[str]:
        combined_relation = [start]
        current = start

        while True:
            next_person = None
            for pair in self.irreducible:
                if pair[0] == current:
                    next_person = pair[1]
                    break
            if next_person:
                combined_relation.append(next_person)
                current = next_person
            else:
                break

        return combined_relation

    def final_ranking(self) -> list[list[str]]:
        self.only_firsts()
        return [self.combine(person) for person in self.only_first]


for arg in sys.argv[1:]:
    section = Relations(arg)
    print(section.final_ranking())
